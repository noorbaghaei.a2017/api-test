<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['namespace'=>'api'],function (){

    Route::post('login', 'ApiController@login')->name('login');

    Route::group(['middleware' => 'auth:api'], function() {

        Route::get('logout', 'ApiController@logout');

        Route::get('admin', 'ApiController@admin')->middleware('can:isAdmin');

        Route::get('user', 'ApiController@user')->middleware('can:isUser');
    });

});

