<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function login(Request $request){
        try {
           $user=User::whereEmail($request->email)->firstOrFail();

            $token = $user->createToken('Personal Access Token');

            $token_result = $token->token;

            $token_result->save();

            return response()->json([
                'access_token' => $token->accessToken,
                'token_type' => 'Bearer',
                'email' => $user->email,
            ]);

        }catch (\Exception $exception){
        return dd($exception->getMessage());
        }
    }
    public function logout(Request $request){
        try {
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out']);

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public function user(Request $request){

        try {
            return response()->json([
                'text'=>'show panel user'
            ]);
        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public function admin(Request $request){

        try {
            return response()->json([
                'text'=>'show panel admin'
            ]);
        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
}
