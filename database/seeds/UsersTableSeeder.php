<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();


        $user=[
            [
                'name'=>"amin",
                'type'=>"admin",
                'email'=>'noorbaghaei.a2017@gmail.com',
                'password'=>Hash::make('secret')
            ],
            [
                'name'=>"hamed",
                'type'=>"user",
                'email'=>'noorbaghaei.a2017@yahoo.com',
                'password'=>Hash::make('secret')
            ],


        ];


        DB::table('users')->insert($user);


    }
}
